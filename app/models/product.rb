class Product < ApplicationRecord
  before_destroy :not_referenced_by_any_line_item
  has_many :line_items
  has_one_attached  :image, :dependent => :destroy
  acts_as_paranoid

  private

  def not_referenced_by_any_line_item
    unless line_items.empty?
      errors.add(:base, "line items present")
      throw :abort
    end
  end




end
