class AddNewFieldsToLineItems < ActiveRecord::Migration[6.0]
  def change
    add_column :line_items, :name, :string
    add_column :line_items, :description, :text
    add_column :line_items, :price, :integer
  end
end
