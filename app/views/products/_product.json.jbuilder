json.extract! product, :id, :name, :description, :price, :created_at, :updated_at, :image
json.url product_url(product, format: :json)
