class AddDeleteToLineItem < ActiveRecord::Migration[6.0]
  def up
    add_column :line_items, :deleted_at, :datetime
    add_index :line_items, :deleted_at
  end

  def down
    remove_column :line_items, :deleted_at
    remove_index :line_items, :deleted_at
  end
end
