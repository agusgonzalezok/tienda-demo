Rails.application.routes.draw do
  resources :line_items
  
  resources :carts do 
    post :terminar
  end

  scope "/admin" do
    resources :products
  end
  
  devise_for :users
  get "admin",to: "backoffice#index"
  root "home#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
