class Cart < ApplicationRecord
  has_many :line_items, dependent: :destroy
  acts_as_paranoid
  include AASM

  aasm column: :state do
    state :activo, initial: true
    state :rechazado
    state :terminado

  
    event :rechazar do
      transitions from: [:activo, :rechazado], to: :rechazado
    end

    event :terminar do
      transitions from: [ :activo, :rechazado], to: :terminado
    end
  end
  
  def add_product(product)
    current_item = line_items.find_by(product_id: product.id)

    if current_item
      current_item.increment(:quantity)

    else 
      current_item = line_items.build(product_id: product.id, name: product.name , description: product.description, price: product.price, deleted_at: nil)

    end
    current_item
  end
  
#Precio total

  def total_price

  line_items.to_a.sum { |items| items.total_price }

  end

end
