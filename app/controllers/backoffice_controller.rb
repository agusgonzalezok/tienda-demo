class BackofficeController < ApplicationController
  
  def index
    @carts = Cart.with_deleted
  end

  def destroy
    @cart = Cart.find_by(id: params[:id])
    @cart.rechazar!
    @cart.destroy
    session[:cart_id] = nil
    
    respond_to do |format|
      format.html { redirect_to admin_path, notice: "Cart was successfully destroyed." }
      format.json { head :no_content }
    end
  end

end
